package com.zuitt;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeNumbers = {2, 3, 5, 7, 11};
        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);

    }
}
