package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class Array {
    //Declaration


    //Multidimensional Array

    public static void main(String[] args) {
        int[] intArray = new int[5];
        // the [] indicates that this int data type should be able to hold multiple int values
        //the "new" keyword is used in non-primitive data types to tell Java to create variable
        //This process is called "Instantiation"
        //The integer value inside  the [] on this side of the statement indicates the amount of integer values the array can hold.

        //Declaration with initialization
        int[] intArray2 = {100, 200, 300, 400, 500};

        String[][] classroom = new String[3][3];

        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";

        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        classroom[2][2] = "Daffy";

        //System.out.println(classroom[2][2]);

        //ArrayList

        ArrayList<String> students = new ArrayList<>();

        //Adding elements to the ArrayList
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //set element at index
        students.set(1, "George");

        //remove element from index
        students.remove(1);

        //Accessing element to the ArrayList
        System.out.println(students);

        //remove all elements in arraylist
        students.clear();
        System.out.println(students);
        System.out.println(students.size());
        System.out.println(classroom[0][1]);


        //Hash Maps
        HashMap<String, String> job_position = new HashMap<>();

        //Adding elements into the HashMap
        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");
        System.out.println(job_position);

        //Accessing elements of the HashMap
        job_position.get("Alice");

        System.out.println(job_position.get("Alice"));

        //Removing elements in HashMap
        //job_position.remove("Brandon");
        System.out.println(job_position);

        //Getting the key of the elements of HashMaps
        System.out.println(job_position.keySet());
        //Operators allow us to manipulate the values that we store in variables. They represent logical and arithmetic operations.

        /*
        * Types of Operators
        *
        * 1. Arithmetic: -,+ , *, /, %
        * 2. Comparison: >, <, >=, <=, ==
        * 3.
        * */


    }




}
