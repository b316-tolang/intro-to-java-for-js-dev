package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        System.out.print("Enter a username: ");
        Scanner myObj = new Scanner(System.in); // Create a Scanner Object

        String username = myObj.nextLine(); // Reads user input
        System.out.println("Username is: " + username);

    }
}
