package com.zuitt;

import java.util.Date;
import java.text.SimpleDateFormat;
public class Course {
    private String name;
    private String description;
    private int seats;
    private double fee;
    private Date startDate;
    private Date endDate;
    private User instructor;

    public Course() {
    }

    public Course(String name, String description, int seats, double fee, Date startDate, Date endDate, User instructor) {
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public User getInstructor() {
        return instructor;
    }

    public void setInstructor(User instructor) {
        this.instructor = instructor;
    }

    public void printCourseInfo() {
        System.out.println("Course: " + this.name);
        System.out.println("Description: " + this.description);
        System.out.println("Instructor: " + this.instructor.getName());
        System.out.println("Slots: " + this.seats);
        System.out.println("Price: P" + this.fee);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        System.out.println("Start date: " + dateFormat.format(this.startDate));
        System.out.println("End date: " + dateFormat.format(this.endDate));
    }
}
