package com.zuitt;
import java.util.Date;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        User janie = new User("Janie Jardie", 30, "owen@gmail.com", "Bulacan");
        Course MCP = new Course();
        MCP.setName("Web Developer Program");
        MCP.setDescription("An intro to Web development using the MERN stack");
        MCP.setFee(60000.00);
        MCP.setInstructor(janie);
        MCP.setSeats(30);
        MCP.setStartDate(new Date(123, 4, 8)); // May 08, 2023
        MCP.setEndDate(new Date(123, 5, 23)); // June 23, 2023

        System.out.println("INSTRUCTOR INFO:");
        System.out.println("Name: " + janie.getName());
        System.out.println("Age: " + janie.getAge());
        System.out.println("Email: " + janie.getEmail());
        System.out.println("Address: " + janie.getAddress());

        System.out.println();

        System.out.println("COURSE INFO: ");
        System.out.println("Course: " + MCP.getName());
        System.out.println("Description: " + MCP.getDescription());
        System.out.println("Instructor: " + MCP.getInstructor().getName());
        System.out.println("Slots: " + MCP.getSeats());
        System.out.println("Price: P" + MCP.getFee());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        System.out.println("Start date: " + dateFormat.format(MCP.getStartDate()));
        System.out.println("End date: " + dateFormat.format(MCP.getEndDate()));
    }
}
