package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
//        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 200000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);

        //Create two new instances of the car class;
//        Car car2 = new Car("Fortuner","Toyota", 20000, new Driver());
//        System.out.println("Car2");
//        System.out.println("Brand: " + car2.brand);
//        System.out.println("Make: "+car2.make);
//        System.out.println("Price: " + car2.price);
//        System.out.println();
//        System.out.println("Car3");
//
//        Car car3 = new Car("Corolla","Toyota", 15000, new Driver());
//        System.out.println("Brand: " + car3.brand);
//        System.out.println("Make: "+car3.make);
//        System.out.println("Price: " + car3.price);

        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Driver driver1 = new Driver("Alejandro", 25);
        System.out.println(driver1.name);

        //property getters
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());

        //property setters
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car2.setMake("Innova");
        System.out.println(car2.getMake());

        //carDriver Getter
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver());

        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);

        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());

        Animal cat = new Animal();
        cat.setName("NickFury");
        cat.setColor("Black");

        System.out.println(cat);
        cat.call();

        Animal animal1 = new Animal("Clifford", "Red");
        animal1.call();

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("Hachiko");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("Brown");
        System.out.println(dog1.getColor());
        dog1.greet();

        Dog dog2 = new Dog("Mike", "Dark brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBreed());
        dog2.greet();

    }
}
