package com.zuitt.example;

public class Dog extends Animal{
    //extends keyword allows us to have this class inherit the attributes and methods of the animal class
    //Parent class is the the class where we inherit from
    //Child class/subclass a class that inherits from a parent
    //No, subclass cannot inherit from multiple parents
    //Instead subclass can multiple inherit from what we call interfaces

    private String dogBreed;

    public Dog() {
        super(); // super is a reference to the parent class constructor
        this.dogBreed = "Chihuahua";
    }

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.dogBreed = breed;
    }

    public String getDogBreed() {
        return dogBreed;
    }

    public void setDogBreed(String dogBreed) {
        this.dogBreed = dogBreed;
    }

    public void greet() {
        super.call();

        System.out.println("Bark!");
    }
}
