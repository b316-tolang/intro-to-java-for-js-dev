package com.zuitt.example;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // The Scanner object looks for which input to scan, in this case it is the console (System.in)
        // The Scanner object has multiple methods that can be used to get the expected value
        /*
        * nextInt()
        * nextDouble()
        * nextLine() - gets the entire line as a String
        * */


        System.out.println("Input a number: ");
        int num = 0;

        try { // try to do this statement
            num = input.nextInt(); // The nextInt() method tells Java that it is expecting an integer value as input.
        } catch (Exception e) {
            System.out.println("Invalid input");
            e.printStackTrace();
        }
    }
}
