package com.zuitt;

import java.util.Scanner;

public class StretchGoal {
    public static void main(String[] args) {

        System.out.println("Input the height for the stars triangle");
        Scanner scanner = new Scanner(System.in);

        int num = 0;
        try {
            num = scanner.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid input. Input must be an integer");
            e.printStackTrace();
        }

        for (int i = 0; i < num; i++) {
/*            for(int j=0; j<i+1; j++) {
                System.out.print("*");
            }*/
            System.out.println("*".repeat(i + 1));
        }
    }
}
