package com.zuitt;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");
        Scanner scanner = new Scanner(System.in);

        int num = 0;

        try {
            num = scanner.nextInt();

            if (num < 0) {
                System.out.println("Please enter a non-negative integer");
            } else if (num >= 0) {
                int answer = 1;
                int counter = 1;

                while (counter <= num) {
                    if (num == 0) {
                        break;
                    }
                    answer *= counter;
                    counter++;
                }
                System.out.println("Using while-loop: The factorial of " + num + " is " + answer);

                answer = 1;
                for (int i = num; i > 1; i--) {
                    answer *= i;
                }
                System.out.println("Using for-loop: The factorial of " + num + " is " + answer);
            }

        } catch (Exception e) {
            System.out.println("Invalid input. Input must be an integer");
            e.printStackTrace();
        }


        for (int i = 0; i < num; i++) {
/*            for (int j = 0; j < i + 1; j++) {
                System.out.print("*");
            }*/

            System.out.println("*".repeat(i + 1));
        }

    }
}
