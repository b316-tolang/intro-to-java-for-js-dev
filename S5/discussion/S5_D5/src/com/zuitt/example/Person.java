package com.zuitt.example;

public class Person implements Actions, Greetings {

    @Override
    public void sleep() {
        System.out.println("Zzzzzzz......");
    }
    @Override

    public void run() {
        System.out.println("Running on the road");
    }

    @Override
    public void eat() {
        System.out.println("Ate something");
    }

    @Override
    public void swim() {
        System.out.println("Swim in th pool");
    }

    @Override
    public void sayHi() {
        System.out.println("Hi There!!");
    }

    @Override
    public void sayGoodbye() {
        System.out.println("GoodBye! Tsuss!");
    }
}
