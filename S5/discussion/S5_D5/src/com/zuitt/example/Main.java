package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person();

        person1.run();
        person1.sleep();
        person1.eat();
        person1.swim();
        System.out.println();
        person1.sayHi();
        person1.sayGoodbye();

        StaticPoly sp = new StaticPoly();

        System.out.println(sp.addition(1,5));
        System.out.println(sp.addition(1,5, 10));
        System.out.println(sp.addition(15.5, 15.6));

        Parent parent1 = new Parent("John", 35);
        parent1.greet();
        parent1.greet("John", "morning");
        parent1.speak();

        Child newChild = new Child();
        newChild.speak();
    }
}
