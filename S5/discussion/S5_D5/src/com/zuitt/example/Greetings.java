package com.zuitt.example;

public interface Greetings {
    public void sayHi();
    public void sayGoodbye();

}
