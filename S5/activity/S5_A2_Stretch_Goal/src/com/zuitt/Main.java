package com.zuitt;
import java.util.Date;
import java.text.SimpleDateFormat;
public class Main {
    public static void main(String[] args) {
        User instructor = new User("Terrence Gaffud", 25, "tgaff@mail.com", "my house in Quezon city");
        System.out.println("Hi! " + "I'm " + instructor.getName() +". I'm " + instructor.getAge() + " years old. You can reach me via my email: " + instructor.getEmail()+". When I'm off work, I can be found at " + instructor.getAddress());

        Course java = new Course();
        java.setName("MACQ004");
        java.setDescription("An Introduction to Java for career-shifters");
        java.setInstructor(instructor);
        java.setSeats(30);
        java.setFee(5_000);
        java.setStartDate(new Date(123, 4, 8));
        java.setEndDate(new Date(123, 5, 23));

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
        System.out.println("=============================================");
        System.out.println("Welcome to the course " + java.getName() + ". This course can be described as " + java.getDescription()+ ". Your instructor for this course is Sir " + java.getInstructor().getName() + ". Enjoy!");
        System.out.println("The price for this course will be " + java.getFee()+" pesos. There are only " + java.getSeats()+ " slots available so register for the course ASAP. The course will start on " + dateFormat.format(java.getStartDate()) + " and will end on " + dateFormat.format(java.getEndDate())  );
    }
}
