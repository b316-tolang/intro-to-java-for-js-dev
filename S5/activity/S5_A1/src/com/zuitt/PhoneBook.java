package com.zuitt;
import java.util.ArrayList;
public class PhoneBook {
    ArrayList<Contact> contacts;

    public PhoneBook() {
        this.contacts = new ArrayList<>();
    }

    public PhoneBook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

    public void printPhoneBook() {
        if(contacts.isEmpty()){
            System.out.println("Phone Book is empty");
            return;
        }

        for(Contact c : contacts){
            System.out.println(c.getName());
            System.out.println("----------------------------------------");
            System.out.println(c.getName() + " has the following registered numbers:");
            System.out.println(c.getContactNumber());
            System.out.println("----------------------------------------");
            System.out.println(c.getName() + " has the following registered addresses:");
            System.out.println(c.getAddress());
            System.out.println("========================================");
        }
    }
}
